"""
    Problem:
        Each new term in the Fibonacci sequence is generated
        by adding the previous two terms. By starting with 1 and 2,
        the first 10 terms will be:
            1, 2, 3, 5, 8, 13, 21, 34, 55, 89, ...

        By considering the terms in the Fibonacci sequence whose values
        do not excedd four million, find the sum of the even-valued terms.

    Origin: Project Euler
    Link: https://projecteuler.net/problem=2

"""


def fibonacci_sequence(n):
    s = [1, 2]
    while (s[-1] + s[-2]) < n:
        s.append(s[-1] + s[-2])
    return s


def total(n):
    sequence = fibonacci_sequence(n)
    only_even = [x for x in sequence if x % 2 == 0]
    return sum(only_even)


if __name__ == "__main__":
    print("Answer: ", total(4000000))


