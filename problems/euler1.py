"""
    Problem:
        If we list all the natural numbers below 10
        that are multiples of 3 or 5, we get 3, 5, 6 and 9.
        The sum of all the multiples of 3 or 5 below 1000

    Origin: Project Euler
    Link: https://projecteuler.net/problem=1

"""


def is_multiple(x):
    return (x % 3 == 0 or x % 5 == 0)


def total(n):
    total = 0
    for i in range(1, n):
        total += (i * is_multiple(i))
    return total


if __name__ == "__main__":
    print("Answer for 10: " + str(total(10)))
    print("Answer for 1000: " + str(total(1000)))

