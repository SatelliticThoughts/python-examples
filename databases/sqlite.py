"""
    SQLite3 Example

    Demonstrates

        - Connecting to database
        - Creating tables
        - Droping tables
        - Inserting data
        - Deleting data
        - Updating data
        - Reading data
"""


import sqlite3


def execute_save(connection, command):
    connection.execute(command)
    connection.commit()


def create_table_users():
    command = """
        CREATE TABLE IF NOT EXISTS Users (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            username TEXT NOT NULL,
            password CHAR(64) NOT NULL
        );
    """
    
    return command


def drop_table_users():
    command = """
        DROP TABLE Users;
    """

    return command


def insert_user(username, password):
    command = """
        INSERT INTO Users(username, password)
        VALUES('{}', '{}');
    """.format(username, password)
    
    return command


def delete_user(user_id):
    command = """
        DELETE FROM Users
        WHERE id = {};
    """.format(user_id)

    return command


def update_user(username, password):
    command = """
        UPDATE Users
        SET password = '{}'
        WHERE username = '{}';
    """.format(password, username)
    
    return command


def read_user(connection):
    command = """
        SELECT id, username, password
        FROM Users;
    """

    cursor = connection.execute(command)
    
    print("Users")
    print("ID\t\tUSERNAME\t\tPASSWORD")
    for row in cursor:
        row_as_str = "{}\t\t{}\t\t\t{}".format(row[0], row[1], row[2])
        print(row_as_str)


if __name__ == "__main__":
    db_name = "myDatabase.db"

    c = sqlite3.connect(db_name)

    execute_save(c, create_table_users())
    
    execute_save(c, insert_user("Tony", "Pass1234"))

    execute_save(c, update_user("Luke", "Super Secret"))
    
    execute_save(c, delete_user(3))

    read_user(c)
    
    c.close()

