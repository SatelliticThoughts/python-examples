#!/usr/bin/env python3
'''
    Simple Personal Assitant.
        - Says hello back.
        - Exits on Goodbye.
        - Writes a message to note.txt.
'''


import speech_recognition
import os

from gtts import gTTS


def listen():
    r = speech_recognition.Recognizer()
    with speech_recognition.Microphone() as source:
        audio = r.listen(source)

    audio_as_text = ""
    try:
        audio_as_text = r.recognize_google(audio)
    except speech_recognition.UnknownValueError:
        print("Error: Could not understand audio")
    except speech_recognition.RequestError as e:
        print("Error: Request error " + e)

    return audio_as_text.lower()
    

def say(audioText):
    tts = gTTS(text=audioText, lang="en")
    tts.save("audio.mp3")
    os.system("mpg123 audio.mp3")


def make_note():
    say("making note")
    f = open("note.txt", "w")
    f.write(listen())
    f.write("\n")
    f.close()
    say("note created")


def main():
    running = True
    while running:
        try:
            say("I'm listening")
            command = listen()
            print(command)

            for word in bye:
                if word in command:
                    running = False

            for word in greetings:
                if word in command:
                    say("Hello")

            for word in notes:
                if word in command:
                    make_note()
        except:
            say("Sorry, I did not understand that.")
    
    say("goodbye")


if __name__ == "__main__":
    greetings = ["hi", "hello"]
    bye = ["bye", "goodbye"]
    notes = ["note"]

    main()

