#!/usr/bin/env python3
'''
    Simple chat application
        - Allows session users
        - login and logout
        - create message and store in array
'''


import json
import flask
from flask import Flask, request, render_template, session, redirect


app = Flask(__name__)
app.secret_key = "top secret"


@app.route("/")
def index():
    if "username" in session:
        return redirect("/chatroom")
    else:
        return redirect("/login")


@app.route("/chatroom")
def chatroom():
    if "username" in session:
        return render_template("chatroom.html", 
                username=session["username"], 
                messages=data["messages"])
    return redirect("/login")


@app.route("/addmessage", methods=["POST"])
def addmessage():
    if "username" in session:
        data["messages"].append({
            "username": session["username"],
            "message": request.form["message"].strip()
            })
        return redirect("/chatroom")
    return redirect("/login")


@app.route("/login", methods=["GET", "POST"])
def login():
    if request.method == "POST":
        session["username"] = request.form["username"].strip()
        
        return redirect("/chatroom")
    else:
        return render_template("login.html")


@app.route("/logout")
def logout():
    session.pop("username", None)
    return redirect("login")


if __name__ == "__main__":
    data = {}
    data["messages"] = []

    app.run(host="10.15.165.240", debug=True)

