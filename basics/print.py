#!/usr/bin/env python3

'''
    This file demonstrates the print statement in python.
    
    A print statement is a method of displaying information to the user.
    A user can't see the code, so displaying instructions or data can be done
        through the print statement
'''


# Hello World example. You can use single or double quotes for strings/text
print("Hello World")
print('Hello World')


# Printing variable example.
text = "This is a test"
print(text)


# Printing multiple variables.
name = "luke"
age = 46

print("My name is", name, "and i am", age, "years old")
