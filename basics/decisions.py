#!/usr/bin/env python3

'''
    The main decision making is if/else statments.
    Conditions in if/else statements boil down to if it's True then run the
        code block, and if it's False skip code block.

    Conditions can compare strings and numbers. Some examples of comparison 
        operators:
        - Equal(==), returns True if both variables are the same.
        - Not Equal(!=), returns True if both variables are different.
        - Greater Than(>), returns True if left variable is larger than right.
        - Less Than(<), Returns True if right variable is smaller than left.

    Conditions can be chained together with and/or. and means both conditions
        must be True for the end result to be True, or means only 1 condition
        must be True for the end result to be True.
'''


# Basic if statement.
if True:
    print("This will show")


# Basic if/else statement.
if False:
    print("This will NOT show")
else:
    print("This will show")


#Basic if/elif/else statement.
if False:
    print("This will NOT show")
elif True:
    print("This will show")
else:
    print("This will NOT show")


# Chained commands with and.
if True and True:
    print("This will show")

if True and False:
    print("This will NOT show")

if False and False:
    print("This will NOT show")


# Chained commands with or.
if True or True:
    print("This will show")

if True or False:
    print("This will show")

if False or False:
    print("This will NOT show")


x = 5
y = 8

# Equals
if x == y:
    print("If x equals y this will show")

# Not Equals
if x != y:
    print("If x does not equal y this will show")

# Greater Than
if x > y:
    print("If x is bigger than y this will show")

# Less Than
if x < y:
    print("If x is smaller than y this will show")


