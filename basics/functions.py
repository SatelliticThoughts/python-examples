#!/usr/bin/env python3

'''
    A function is a block of reusable code.
    
    Functions return information, if no information is given a function by 
        default returns None. Functions can return variables and objects.
        
    Functions can take in variables. variables passed into functions are called
    parameters.
'''


# Basic function with no return statement or parameters
def say_hello():
    print("Hello")


# Basic function returning a string
def get_word():
    return "Word"


# Basic function with single parameter
def double_number(x):
    print(x * x)


# Basic function with multiple parameters and return statement
def add_numbers(x, y):
    return x + y


# Functions being called and executed
say_hello()
print(get_word())
double_number(88)
print(add_numbers(45, 29))
