#!/usr/bin/env python3

'''
    Loops execute a block of code until loop's condition becomes False.

    Some Loop Types are while and for.

    while loops are general purpose

    for loops are suited better for iterating over a list. 
'''


running = True
i = 0

# Basic while loop
while running:
    i += 1
    if i > 5:
        running = False


numbers = [3, 2, 7, 8, 1, 0, 3]

# Basic for loop, iterating over list of random numbers
for number in numbers:
    print(number)

# Basic for loop iterating from 0 to 12
for i in range(0, 12):
    print("5 +", i, "=", 5 * i)

