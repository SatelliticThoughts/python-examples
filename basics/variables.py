#!/usr/bin/env python3

'''
    Variable store information, that can be system or user defined.

    Variable Types:
        - integer, whole numbers
        - floating point, decimal numbers
        - string, text stored in between quotations(single or double)
        - boolean, True or False
        - lists, contains multiple variables in one 
'''


# Integers, and some calculations that can be performed with them
x = 48
y = 12
print("Integer Calculations, x =", x, ", y =", y)

print("x + y =", x + y) # Addition
print("x - y =", x - y) # Subtraction
print("x * y =", x * y) # Multiplication
print("x / y =", x / y) # Division
print("x ** 2 =", x ** 2) # Power
print("y % x =", x % y) # Modulus


# Floating point numbers, and some calculations that can be performed with them
x = 8.327
y = 2.45
print("\nFloating point Calculations, x =", x, ", y =", y)

print("x + y =", x + y) # Addition
print("x - y =", x - y) # Subtraction
print("x * y =", x * y) # Multiplication
print("x / y =", x / y) # Division
print("x ** 2 =", x ** 2) # Power
print("y % x =", x % y) # Modulus


# Strings, and some manipulation that can be done on them
print("\nString manipulation")

hello = "hello"
print(hello) # Simple string

hello = hello + " world "
print(hello) # String addition

hello = hello * 4
print(hello) # String multiplication

print(hello[2: 8]) # Substring
print(hello[::-1]) # Reverse String


# Lists, and some manipulation that can be done on them
numbers = [4, 7, 3, 1, 2, 3]
print("\nList Manipulation")

print(numbers)
print(len(numbers))

numbers.append(99)
print(numbers)

numbers.remove(1)
print(numbers)


