#!/usr/bin/env python3

'''
    Objects can group functions and variables together.
    Constructor, is what's called when object is instantiated
    Getter, return property's value
    Setter, sets property's value
'''


class Person:
    # Constructor, sets default values if none are given
    def __init__(self, name="", age=0):
        self.name = name # Calls setter for name
        self.age = age # Calls setter for age

    # Getter for name
    @property
    def name(self):
        return self.__name

    # Setter for name
    @name.setter
    def name(self, name):
        self.__name = name

    # Getter for age
    @property
    def age(self):
        return self.__age

    # Setter for age
    @age.setter
    def age(self, age):
        self.__age = age


# Instantiating Person Object
p = Person("luke", 34)

# Printing property values with getters
print(p.name, p.age)
