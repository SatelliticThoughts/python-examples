# python-examples

python-examples contains code to demonstrate various things in Python.

## Set up

After cloning or downloading, most programs should be a single file that can be run with python3, unless otherwise stated.

```
python3 'filename.py' # run file
```

## License
[MIT](https://choosealicense.com/licenses/mit/)
